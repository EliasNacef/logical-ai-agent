#include <InferenceEngine/InferenceEngine.h>

InferenceEngine::InferenceEngine(std::string jsonPath)
{
	std::ifstream rules_file;
	rules_file.open(jsonPath);
	json rules = json::parse(rules_file);

	for (const auto& rule : rules.items())
	{
		std::string ruleName = rule.key().c_str();

		std::string condition = rule.value()["condition"];

		for (const auto& consequence : rule.value()["consequence"].items())
		{
			std::string what = consequence.value()["what"];

			// Consequence found
			Fact fact = Fact(); // empty fact

			if (what == "monster")
			{
				// Monster consequence found
				fact.SetMonsterAround(true);
			}

			if (what == "crack")
			{
				// Crack consequence found
				fact.SetCrackAround(true);
			}

			std::vector<Position> offsets;
			for (const auto& direction : consequence.value()["direction"])
			{
				std::string xString = direction["x"];
				std::string yString = direction["y"];
				int x = std::stoi(xString);
				int y = std::stoi(yString);
				offsets.push_back(Position(x, y));
			}
			fact.SetOffsets(offsets);

			// Push back a new rule
			Rule rule(condition, fact);
			m_rules.push_back(rule);
		}
	}
}

std::vector<Fact> InferenceEngine::ConvertToInferenceFacts(std::vector<AreaFacts> allFacts)
{
	std::vector<Fact> facts;
	for (AreaFacts areaFacts : allFacts)
	{
		const auto& pos = areaFacts.GetPosition();
		const auto& smell = areaFacts.IsThereSmell();
		const auto& wind = areaFacts.IsThereWind();
		int factX = pos.GetX();
		int factY = pos.GetY();
		Fact fact(pos, smell, wind);
		facts.push_back(fact);
	}
	return facts;
}

std::vector<Rule> InferenceEngine::SelectRules(const Fact& fact)
{
	std::vector<Rule> applicableRules;
	for (auto& rule : m_rules)
	{
		if (!rule.IsRuleTag())
		{
			std::string condition = rule.GetCondition();
			if (condition == "smell" && fact.GetMonsterAround())
			{
				applicableRules.push_back(rule);
			}
			else if (condition == "wind" && fact.GetCrackAround())
			{
				applicableRules.push_back(rule);
			}
			//rule.TagRule();
		}
	}
	return applicableRules;
}

Fact InferenceEngine::ApplyRule(Rule& rule, const Position& position)
{
	Fact consequence = rule.GetConsequence();
	consequence.SetPosition(position);
	rule.TagRule();
	return consequence;
}

std::vector<AreaFacts> InferenceEngine::FrontChaining(std::vector<AreaFacts> areaFacts)
{
	// Conversion in Inference Fact
	std::vector<Fact> inferenceFacts = ConvertToInferenceFacts(areaFacts);

	std::vector<Fact> consequencesFacts;
	for (auto& fact : inferenceFacts)
	{
		std::vector<Rule> applicableRules = SelectRules(fact);
		for (auto& rule : applicableRules)
		{
			auto factPosition = fact.GetPosition();
			consequencesFacts.push_back(ApplyRule(rule, factPosition));
		}
	}

	for (Fact& fact : consequencesFacts)
	{
		const auto& pos = fact.GetPosition();
		const auto& offsets = fact.GetOffsets();
		for (auto& offset : offsets)
		{
			Position offsetPos = Position(pos.GetX() + offset.GetX(), pos.GetY() + offset.GetY());
			auto it = find_if(areaFacts.begin(), areaFacts.end(), [&offsetPos](const AreaFacts& obj) {return obj.GetPosition() == offsetPos; });
			if (it != areaFacts.end())
			{
				// Already exist : update fact
				if (fact.GetCrackAround())
				{
					it->IncrementCrackQuantification();
				}
				if (fact.GetMonsterAround())
				{
					it->IncrementMonsterQuantification();
				}
			}
			else
			{
				// Create fact
				AreaFacts offsetConsequence(offsetPos, false, false, 0, 0);
				if (fact.GetCrackAround())
				{
					offsetConsequence.IncrementCrackQuantification();
				}
				if (fact.GetMonsterAround())
				{
					offsetConsequence.IncrementMonsterQuantification();
				}
				areaFacts.push_back(offsetConsequence);
			}
		}
	}

	return areaFacts;
}