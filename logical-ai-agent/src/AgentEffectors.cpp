#include <Agent\AgentEffectors.h>


AgentEffectors::AgentEffectors() : m_envSize(0), m_intention(ActionType::NONE)
{
}

int AgentEffectors::Use(const Action& desire, Position& position, bool& isShooting)
{
	return Do(desire, position, isShooting);
}

int AgentEffectors::Do(const Action& desire, Position& position, bool& isShooting)
{
	int performance = 0;
	UpdateIntention(desire);
	const auto& destination = m_intention.GetDestination();

	switch(m_intention.GetType())
	{
	case ActionType::MOVE: 
		Move(isShooting, position, destination);
		performance++;
		break;
	case ActionType::SHOOTANDMOVE: 
		ShootAndMove(isShooting, position, destination);
		performance += 10;
		break;
	default:
		// DO NOTHING
		break;
	}

	return performance;
}

void AgentEffectors::UpdateIntention(const Action& action)
{
	m_intention = action;
}

void AgentEffectors::Move(bool& isShooting, Position& from, const Position& destination)
{
	isShooting = false;
	if (!IsValid(destination)) return;
	int x = destination.GetX();
	int y = destination.GetY();
	from.SetX(x);
	from.SetY(y);
}

void AgentEffectors::ShootAndMove(bool& isShooting, Position& from, const Position& destination)
{
	isShooting = true;
	if (!IsValid(destination)) return;
	int x = destination.GetX();
	int y = destination.GetY();
	from.SetX(x);
	from.SetY(y);
}

bool AgentEffectors::IsValid(const Position& position)
{
	int x = position.GetX();
	int y = position.GetY();
	return (x >= 0 && y >= 0 && x < m_envSize && y < m_envSize);
}
