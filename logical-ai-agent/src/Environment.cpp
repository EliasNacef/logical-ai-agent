#include <Environment/Environment.h>

Environment::Environment(int n, Agent* agent) : m_size(n), m_agent(agent)
{
	agent->SetEnvironmentSize(n);
	m_currentAgentPosition = agent->GetPosition();
	for (int x = 0; x < m_size; x++)
	{
		std::vector<Area> xLine;
		for (int y = 0; y < m_size; y++)
		{
			xLine.push_back(Area(Position(x, y)));
		}
		m_areas.push_back(xLine);
	}

	// Delete not valid cracks and monsters
	const Position& agentPosition = agent->GetPosition();
	int agentX = agentPosition.GetX();
	int agentY = agentPosition.GetY();
	m_areas[agentX][agentY].DeleteCrack();
	m_areas[agentX][agentY].DeleteMonster();

	srand(time(NULL));
	int xPortalArea = rand() % m_size;
	int yPortalArea = rand() % m_size;
	while (xPortalArea == agentX && yPortalArea == agentY)
	{
		xPortalArea = rand() % m_size;
		yPortalArea = rand() % m_size;
	}

	m_areas[xPortalArea][yPortalArea].DeleteCrack();
	m_areas[xPortalArea][yPortalArea].DeleteMonster();
	m_areas[xPortalArea][yPortalArea].ThereIsPortal();
	AddSmellAndWind();
}

void Environment::AddSmellAndWind()
{
	for (int x = 0; x < m_size; x++)
	{
		for (int y = 0; y < m_size; y++)
		{
			// Smell adding
			bool monster = m_areas[x][y].IsThereMonster();
			if (monster)
			{
				if (x > 0)
				{
					m_areas[x - 1][y].ThereIsMonsterSmell();
				}
				if (x < m_size - 1)
				{
					m_areas[x + 1][y].ThereIsMonsterSmell();
				}
				if (y > 0)
				{
					m_areas[x][y - 1].ThereIsMonsterSmell();
				}
				if (y < m_size - 1)
				{
					m_areas[x][y + 1].ThereIsMonsterSmell();
				}
			}

			// Wind adding
			bool crack = m_areas[x][y].IsThereCrack();
			if (crack)
			{
				if (x > 0)
				{
					m_areas[x - 1][y].ThereIsCrackWind();
				}
				if (x < m_size - 1)
				{
					m_areas[x + 1][y].ThereIsCrackWind();
				}
				if (y > 0)
				{
					m_areas[x][y - 1].ThereIsCrackWind();
				}
				if (y < m_size - 1)
				{
					m_areas[x][y + 1].ThereIsCrackWind();
				}
			}
		}
	}
}

void Environment::Tick()
{
	UpdateAgentPosition();
}

void Environment::UpdateAgentPosition()
{
	// Clear old area
	const auto& oldAgentPosition = m_currentAgentPosition;
	auto& oldArea = m_areas[oldAgentPosition.GetX()][oldAgentPosition.GetY()];
	oldArea.AgentIsGone();

	// Update new area
	m_currentAgentPosition = m_agent->GetPosition();

	auto& newArea = m_areas[m_currentAgentPosition.GetX()][m_currentAgentPosition.GetY()];
	newArea.AgentIsHere();

	ArriveOnArea();
}

bool Environment::IsPositionValid(const Position& pos) const
{
	int x = pos.GetX();
	int y = pos.GetY();
	return((x >= 0 && x < m_size) && (y > 0 && y < m_size));
}


void Environment::ArriveOnArea()
{
	int x = m_currentAgentPosition.GetX();
	int y = m_currentAgentPosition.GetY();
	bool monster = m_areas[x][y].IsThereMonster();
	bool crack = m_areas[x][y].IsThereCrack();
	bool portal = m_areas[x][y].IsTherePortal();
	if (m_agent->IsShooting())
	{
		std::cout << "Agent has launched a rock on position " << m_currentAgentPosition.ToString() << std::endl;
		m_areas[x][y].KillMonster();
	}
	if (monster)
	{
		if(!m_agent->IsShooting())
		{
			m_agent->Kill();
		}
	}
	if (crack)
	{
		m_agent->Kill();
	}
	else if (portal)
	{
		m_agent->LeaveForest();
	}
}


void Environment::Disp()
{
	for (int y = m_size - 1; y >= 0; y--)
	{
		DispLine();
		for (int x = 0; x < m_size; x++)
		{
			const auto& area = m_areas[x][y];
			area.Disp();
		}
		std::cout << std::endl;
	}
	DispLine();
}

void Environment::DispLine() const
{
	for (int i = 0; i < m_size; i++)
	{
		std::cout << "--------";
	}
	std::cout << std::endl;
}
