#include <Agent/Agent.h>

Agent::Agent(std::string rules, Position position, int performance) :
	m_sensors(AgentSensors(rules)),
	m_desire(ActionType::NONE),
	m_currentAgentPosition(position),
	m_performance(performance)
{
}

void Agent::Tick(const Area& area)
{
	UseSensors(area);
	m_desire = m_sensors.GetDesire();
	UseEffectors();
}

void Agent::UseSensors(const Area& area)
{
	return m_sensors.Use(m_currentAgentPosition, area);
}

void Agent::UseEffectors()
{
	m_performance -= m_effectors.Use(m_desire, m_currentAgentPosition, m_isShooting);
}

void Agent::Kill()
{
	m_isAlive = false;

	// Performance
	int size = m_sensors.GetEnvironmentSize();
	m_performance -= (10 * size * size);
}

void Agent::LeaveForest()
{
	int size = m_sensors.GetEnvironmentSize();

	// Performance
	m_performance += (10 * size * size);
	m_inForest = false;
}
