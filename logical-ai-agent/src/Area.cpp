#include <Environment\Area.h>

Area::Area(Position position) : m_position(position), m_monsterPresence(false), m_monsterSmell(false), m_crackPresence(false), m_crackWind(false), m_portalLight(false), m_portalPresence(false), m_agentInTheArea(false)
{
	std::random_device rd;
	std::mt19937 mt(rd());
	std::uniform_real_distribution<double> dist(0, 100);

	int monsterProb = dist(mt);
	int crackProb = dist(mt);
	if (monsterProb < 10)
	{
		m_monsterPresence = true;
	}
	if (crackProb < 10)
	{
		m_crackPresence = true;
	}
}

void Area::Disp() const
{
	std::cout << "|";
	// Agend disp
	if (m_agentInTheArea)
	{
		std::cout << "A";
	}
	else
	{
		std::cout << " ";
	}

	// Portal disp
	if (m_portalPresence)
	{
		std::cout << "P";
	}
	else
	{
		std::cout << " ";
	}

	// Monster disp
	if (m_monsterPresence)
	{
		std::cout << "M";
	}
	else
	{
		std::cout << " ";
	}
	if (m_monsterSmell)
	{
		std::cout << "m";
	}
	else
	{
		std::cout << " ";
	}

	// Crack disp
	if (m_crackPresence)
	{
		std::cout << "C";
	}
	else
	{
		std::cout << " ";
	}
	if (m_crackWind)
	{
		std::cout << "c";
	}
	else
	{
		std::cout << " ";
	}
	std::cout << "|";
}
