#include <Agent/AgentSensors.h>

void AgentSensors::Use(const Position& currentPosition, const Area area)
{
	m_currentAgentPosition = currentPosition;
	UpdateBeliefs(area);
	UpdateDesire();
}

void AgentSensors::UpdateBeliefs(const Area area)
{
	ObserveArea(area);
	UpdateFringe(area);
	ResetAreaFacts();
	InferAreaFacts();
}

void AgentSensors::ObserveArea(const Area& area)
{
	// Area is now known if it was not
	const auto& areaPosition = area.GetPosition();
	for (size_t areaIndex = 0; areaIndex < m_knownAreas.size(); areaIndex++)
	{
		const auto& knownPos = m_knownAreas[areaIndex].GetPosition();
		if (knownPos == areaPosition)
		{
			m_knownAreas[areaIndex] = area; // Just update area
			m_knownAreas[areaIndex].AgentIsGone(); // Do not care about agent presence in known area
			return; // Known areas and Fringe do not update
		}
	}

	// Add new known area
	Area copyArea(area);
	copyArea.AgentIsGone();
	m_knownAreas.push_back(copyArea);
}

void AgentSensors::UpdateFringe(const Area& area)
{
	auto aroundPositions = GetAround(area);
	std::vector<Position>::iterator it;
	for (it = aroundPositions.begin(); it != aroundPositions.end(); ++it)
	{
		auto fringeRes = std::find(m_fringePositions.begin(), m_fringePositions.end(), *it);
		if (fringeRes == m_fringePositions.end())
		{
			// Area position not found in current fringe
			m_fringePositions.push_back(*it);
		}
	}
	// Remove known positions from fringe
	for (size_t areaIndex = 0; areaIndex < m_knownAreas.size(); areaIndex++)
	{
		const auto& knownPos = m_knownAreas[areaIndex].GetPosition();
		m_fringePositions.erase(std::remove(m_fringePositions.begin(), m_fringePositions.end(), knownPos), m_fringePositions.end());
	}
}

void AgentSensors::ResetAreaFacts()
{
	// Clear facts
	m_areaFacts.clear();

	for (const auto& area : m_knownAreas)
	{
		AreaFacts fact = AreaFacts(area.GetPosition(), area.IsThereMonsterSmell(), area.IsThereCrackWind(), false, false);
		m_areaFacts.push_back(fact);
	}

	//std::vector<Position>::iterator it;
	//for(auto& fringePos: m_fringePositions)
	//{
	//	// Around analysis
	//	const auto& around = GetAround(fringePos);
	//	int smell = around.size();
	//	int wind = around.size();
	//	for (auto& aroundPos : around)
	//	{
	//		auto it = find_if(m_knownAreas.begin(), m_knownAreas.end(), [&aroundPos](const Area& obj) {return obj.GetPosition() == aroundPos; });
	//		if (it != m_knownAreas.end())
	//		{
	//			// An area at aroundPos position has been found in known areas
	//			auto i = std::distance(m_knownAreas.begin(), it);
	//			if (!m_knownAreas[i].IsThereMonsterSmell())
	//			{
	//				// Monster around
	//				smell--;
	//			}
	//			if (!m_knownAreas[i].IsThereCrackWind())
	//			{
	//				// Crack around
	//				wind--;
	//			}
	//		}
	//	}

	//	// Fact adding
	//	AreaFacts fact = AreaFacts(fringePos, smell, wind);
	//	m_fringeFacts.push_back(fact);
	//}
}

void AgentSensors::InferAreaFacts()
{
	// Use inference engine to update facts (beliefs)
	m_inferedFacts = m_engine.FrontChaining(m_areaFacts);
}

void AgentSensors::UpdateDesire()
{
	if (m_fringePositions.size() <= 0)
	{
		m_desire = GetRandomAction();
		return;
	}

	auto interestPair = std::pair<AreaFacts, float>(m_areaFacts[0], 0.0f);
	for (auto& fringePos : m_fringePositions)
	{
		auto it = find_if(m_inferedFacts.begin(), m_inferedFacts.end(), [&fringePos](const AreaFacts& obj) {return obj.GetPosition() == fringePos; });
		if (it != m_inferedFacts.end())
		{
			// There is a fact about fringe pos area
			float proba = GetFactProbability(*it);
			if (interestPair.second < proba)
			{
				auto i = std::distance(m_inferedFacts.begin(), it);
				interestPair = std::pair<AreaFacts, float>(m_inferedFacts[i], proba);
			}
		}
		else
		{
			m_desire = Action(ActionType::MOVE, fringePos);
			return;
		}
	}
	m_desire = GetFactDesire(interestPair.first);
}

float AgentSensors::GetFactProbability(const AreaFacts& areaFacts) const
{
	int quantMonster = areaFacts.GetMonsterQuantification();
	int quantCrack = areaFacts.GetCrackQuantification();
	float proba = 1.0f;
	proba -= quantMonster * 0.05f;
	proba -= quantCrack * 0.2f;
	return proba;
}

const Action& AgentSensors::GetFactDesire(const AreaFacts& areaFacts) const
{
	int quantMonster = areaFacts.GetMonsterQuantification();
	const Position& destination = areaFacts.GetPosition();
	if (quantMonster > 1)
	{
		// Probable monster
		return Action(ActionType::SHOOTANDMOVE, destination);
	}
	else if (quantMonster == 1)
	{
		// Possible monster : one chance out of two to pass
		int rng = rand() % 2;
		if (rng > 0)
		{
			return Action(ActionType::MOVE, destination);
		}
		else
		{
			return Action(ActionType::SHOOTANDMOVE, destination);
		}
	}
	else
	{
		// No monster
		return Action(ActionType::MOVE, destination);
	}
}

const Action& AgentSensors::GetRandomAction() const
{
	auto pos = m_currentAgentPosition;
	int x = m_currentAgentPosition.GetX();
	int y = m_currentAgentPosition.GetY();
	// Random
	srand(time(NULL));
	int rngShoot = rand() % 2;
	int rngChoice = rand() % 4;
	switch (rngChoice)
	{
	case 0:
		if (rngShoot > 0)
		{
			return Action(ActionType::MOVE, Position(x + 1, y));
		}
		else
		{
			return Action(ActionType::SHOOTANDMOVE, Position(x + 1, y));
		}
		break;
	case 1:
		if (rngShoot > 0)
		{
			return Action(ActionType::MOVE, Position(x - 1, y));
		}
		else
		{
			return Action(ActionType::SHOOTANDMOVE, Position(x - 1, y));
		}
		break;
	case 2:
		if (rngShoot > 0)
		{
			return Action(ActionType::MOVE, Position(x, y + 1));
		}
		else
		{
			return Action(ActionType::SHOOTANDMOVE, Position(x, y + 1));
		}
		break;
	case 3:
		if (rngShoot > 0)
		{
			return Action(ActionType::MOVE, Position(x, y - 1));
		}
		else
		{
			return Action(ActionType::SHOOTANDMOVE, Position(x, y - 1));
		}
		break;
	default:
		if (rngShoot > 0)
		{
			return Action(ActionType::MOVE, Position(x, y));
		}
		else
		{
			return Action(ActionType::SHOOTANDMOVE, Position(x, y));
		}
		break;
	}
}


bool AgentSensors::IsAlreadyKnown(const Position& position) const
{
	for (size_t area = 0; area < m_knownAreas.size(); area++)
	{
		if (m_knownAreas[area].GetPosition() == position)
		{
			return true;
		}
	}
	return false;
}

const Area& AgentSensors::GetArea(const Position& position) const
{
	for (size_t areaIndex = 0; areaIndex < m_knownAreas.size(); areaIndex++)
	{
		const auto& areaPos = m_knownAreas[areaIndex].GetPosition();
		if (areaPos == position)
		{
			return m_knownAreas[areaIndex];
		}
	}
	throw "Bad Area position asked !";
}

std::vector<Position> AgentSensors::GetAround(const Area& area) const
{
	const auto& areaPos = area.GetPosition();
	return GetAround(areaPos);
}

std::vector<Position> AgentSensors::GetAround(const Position& areaPos) const
{
	std::vector<Position> aroundPositions;
	const int x = areaPos.GetX();
	const int y = areaPos.GetY();
	if (x > 0)
	{
		// Left area
		aroundPositions.push_back(Position(x - 1, y));
	}
	if (x < m_envSize - 1)
	{
		// Right area
		aroundPositions.push_back(Position(x + 1, y));
	}
	if (y > 0)
	{
		// Down area
		aroundPositions.push_back(Position(x, y - 1));
	}
	if (y < m_envSize - 1)
	{
		// Up area
		aroundPositions.push_back(Position(x, y + 1));
	}
	return aroundPositions;
}