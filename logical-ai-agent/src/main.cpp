#include <iostream>
#include <thread>
#include <common.h>
#include <Environment/Environment.h>
#include <Agent/Agent.h>
#include <nlohmann\json.hpp>


void Tick(Environment& env, Agent* agent)
{
    // Disp Clean
    system("CLS");

    // Agent Desire Disp
    std::cout << "Agent desire / intention : " << std::endl;
    agent->DispDesire();
    std::cout << std::endl;
    std::cout << "Agent performance : " << agent->GetPerformance() << std::endl;
    std::cout << std::endl;

    // Env Handling
    env.Tick();
    // Agent Handling
    agent->Tick(env.GetArea(agent->GetPosition()));

    // Env Disp
    env.Disp();
    std::cout << std::endl;

    // Agent Knowledge Disp
    std::cout << "Agent areas known : " << std::endl;
    const auto& areas = agent->GetKnownAreas();
    for (size_t i = 0; i < areas.size(); i++)
    {
        areas[i].Disp();
    }
    std::cout << std::endl;
}

void RunForest(int n, std::string rulesJson, int perf)
{
    // Disp Clean
    system("CLS");

    //cr�ation agent
    Position initialAgentPos(0, n - 1);
    auto agent = std::make_shared<Agent>(rulesJson, initialAgentPos, perf);

    //cr�ation env
    Environment env(n, agent.get());

    auto start = std::chrono::high_resolution_clock::now();
    auto end = start;
    std::chrono::microseconds chrono = 0ms;
    while (agent->IsAlive() && agent->InForest())
    {
        Tick(env, agent.get());
        std::cin.ignore();
    }

    if (!agent->IsAlive())
    {
        // Disp
        std::cout << std::endl;
        std::cout << "Agent is dead ! Reset !" << std::endl;

        // Wait and rerun
        std::cin.ignore();
        RunForest(n, rulesJson, agent->GetPerformance());
    }
    else if (!agent->InForest())
    {
        std::cout << std::endl;
        std::cout << "Agent has succeed to leave forest ! Reset with " << n + 1 << "x" << n + 1 << " sized forest !" << std::endl;

        // Wait and rerun
        std::cin.ignore();
        RunForest(n + 1, rulesJson, agent->GetPerformance());
    }
}

int main(int argc, char* argv[])
{
    if (argc != 2)
    {
        std::cout << "The program need only one argument : the rules file in a json format !" << std::endl;
    }
    // Run forest
    RunForest(3, argv[1], 0);

    std::cin.ignore();
    return EXIT_SUCCESS;
}