#pragma once
#include <iostream>
#include <string>

class Position
{
public:

    Position() : m_x(0), m_y(0) {}
    Position(int x, int y) : m_x(x), m_y(y) {}

    int GetX() const
    {
        return m_x;
    }

    int GetY() const
    {
        return m_y;
    }

    std::string ToString() const
    {
        return "(x = " + std::to_string(m_x) + ", y = " + std::to_string(m_y) + ")";
    }

    void SetX(int x)
    {
        m_x = x;
    }

    void SetY(int y)
    {
        m_y = y;
    }

    static int ManhattanDistance(Position a, Position b)
    {
        return abs(a.GetX() - b.GetX()) + abs(a.GetY() - b.GetY());
    }

    bool operator==(const Position& p) const
    {
        return m_x == p.GetX() && m_y == p.GetY();
    }

private: 
    int m_x;
    int m_y;
};

