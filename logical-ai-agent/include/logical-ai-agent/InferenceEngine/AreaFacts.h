#pragma once

#include <iostream>
#include <Position.h>

/// <summary>
/// Define a fact for the inference engine
/// </summary>
class AreaFacts
{
public:
	/// <summary>
	/// Ctor
	/// </summary>
	AreaFacts(std::string name) : m_name(name), m_position(Position(0,0)), m_smell(false), m_monsterQuant(0), m_wind(false), m_crackQuant(0) {}

	AreaFacts(Position pos, bool smell, bool wind, int monsterQuant, int crackQuant) :
		m_name(""),
		m_position(pos),
		m_smell(smell),
		m_wind(wind),
		m_monsterQuant(monsterQuant),
		m_crackQuant(crackQuant) {}

	AreaFacts(Position pos, bool smell, bool wind) :
		m_name(""),
		m_position(pos), 
		m_smell(smell), 
		m_wind(wind),
		m_monsterQuant(0),
		m_crackQuant(0){}

	const Position& GetPosition() const
	{
		return m_position;
	}

	int GetMonsterQuantification() const
	{
		return m_monsterQuant;
	}

	int GetCrackQuantification() const
	{
		return m_crackQuant;
	}

	bool IsThereWind() const
	{
		return m_wind;
	}

	bool IsThereSmell() const
	{
		return m_smell;
	}

	void SetPosition(const Position& position)
	{
		m_position = position;
	}

	void IncrementMonsterQuantification()
	{
		m_monsterQuant++;
	}

	void IncrementCrackQuantification()
	{
		m_crackQuant++;
	}

	void Disp() const
	{
		std::cout << "x : ";
		std::cout << m_position.GetX();
		std::cout << " | y : ";
		std::cout << m_position.GetY();
		std::cout << " | smell : ";
		std::cout << m_smell;
		std::cout << " | wind : ";
		std::cout << m_wind;
		std::cout << std::endl;
	}

private:
	std::string m_name;
	Position m_position;
	bool m_smell;
	bool m_wind;
	int m_monsterQuant;
	int m_crackQuant;
};