#pragma once

#include <iostream>
#include <vector>
#include <fstream>
#include <iomanip>
#include <nlohmann/json.hpp>
#include <random>
#include <InferenceEngine/Fact.h>
#include <InferenceEngine/AreaFacts.h>
#include <InferenceEngine/Rule.h>


/// <summary>
/// Define an Inference Engine
/// </summary>
class InferenceEngine
{
public:

	/// <summary>
	/// Ctor
	/// </summary>
	InferenceEngine(std::string jsonPath);

	std::vector<Fact> ConvertToInferenceFacts(std::vector<AreaFacts> allFacts);

	/// <summary>
	/// 
	/// </summary>
	std::vector<Rule> SelectRules(const Fact& fact);

	/// <summary>
	/// 
	/// </summary>
	Fact ApplyRule(Rule& rule, const Position& position);

	/// <summary>
	/// 
	/// </summary>
	std::vector<AreaFacts> FrontChaining(std::vector<AreaFacts> facts);

private:
	std::vector<Rule> m_rules;
};