#pragma once

#include <iostream>
#include <vector>
#include<string>
#include <InferenceEngine/AreaFacts.h>


using json = nlohmann::json;


/// <summary>
/// Define a rule for the inference engine
/// </summary>
class Rule
{
public:
	/// <summary>
	/// Ctor
	/// </summary>
	/// <param name="conditions"> Rule conditions </param>
	/// <param name="consequencies"> Rule consequencies </param>
	Rule(std::string condition, Fact consequence) : m_condition(condition), m_consequences(consequence), m_tag(false), m_applicable(false)
	{
	}

	/// <summary>
	/// Tag a rule
	/// </summary>
	void TagRule()
	{
		m_tag = true;
	}

	/// <summary>
	/// Tell if the rule is already tag or not
	/// </summary>
	/// <returns> Boolean : true if already tag, false if not </returns>
	bool IsRuleTag()
	{
		return m_tag;
	}

	/// <summary>
	/// Tell if the rule is applicable
	/// </summary>
	/// <returns> Boolean : true if applicable, false if not </returns>
	bool IsRuleApplicable()
	{
		return m_applicable;
	}

	std::string GetCondition()
	{
		return m_condition;
	}

	Fact GetConsequence()
	{
		return m_consequences;
	}


private:
	std::string m_condition;
	Fact m_consequences;
	bool m_tag;
	bool m_applicable;
};