#pragma once

#include <iostream>
#include <vector>
#include <Position.h>

/// <summary>
/// Define a fact for the inference engine
/// </summary>
class Fact
{
public:

	Fact(Position pos, bool smell, bool wind) :
		m_position(pos),
		m_monsterAround(smell),
		m_crackAround(wind)
	{}

	Fact() : Fact(Position(0,0), false, false)
	{}


	const Position& GetPosition() const
	{
		return m_position;
	}

	bool GetCrackAround() const
	{
		return m_crackAround;
	}

	bool GetMonsterAround() const
	{
		return m_monsterAround;
	}

	std::vector<Position> GetOffsets() const
	{
		return m_offset;
	}

	void SetPosition(const Position& position)
	{
		m_position = position;
	}

	void SetOffsets(std::vector<Position> offset)
	{
		m_offset = offset;
	}

	void SetMonsterAround(bool value)
	{
		m_monsterAround = value;
	}

	void SetCrackAround(bool value)
	{
		m_crackAround = value;
	}

	void Disp() const
	{
		std::cout << "x : ";
		std::cout << m_position.GetX();
		std::cout << " | y : ";
		std::cout << m_position.GetY();
		std::cout << " | smell : ";
		std::cout << m_monsterAround;
		std::cout << " | wind : ";
		std::cout << m_crackAround;
		std::cout << std::endl;
	}

private:
	Position m_position;
	std::vector<Position> m_offset;
	bool m_monsterAround;
	bool m_crackAround;
};