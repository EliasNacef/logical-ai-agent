#pragma once

#include <agent/AgentSensors.h>
#include <agent/AgentEffectors.h>
#include <Position.h>
#include <InferenceEngine/InferenceEngine.h>

/// <summary>
///  Agent class
/// </summary>
class Agent
{
public:

	/// <summary>
	/// ctor
	/// </summary>
	Agent(std::string rules, Position position, int performance);
	Agent(const Agent&) = delete;
	Agent(Agent&&) = delete;

	void SetEnvironmentSize(int size)
	{
		m_sensors.SetEnvironmentSize(size);
		m_effectors.SetEnvironmentSize(size);
	}

	/// <summary>
	/// Update agent one time
	/// </summary>
	void Tick(const Area& area);

	/// <summary>
	/// Agent uses its sensors
	/// </summary>
	/// <param name="area"> area to observe </param>
	void UseSensors(const Area& area);

	/// <summary>
	/// Agent uses its effectors according to current beliefs and desires
	/// </summary>
	void UseEffectors();

	/// <summary>
	/// Return the position of the agent
	/// </summary>
	/// <returns>the position of the agent</returns>
	const Position& GetPosition() const
	{
		return m_currentAgentPosition;
	}

	std::vector<Area> GetKnownAreas() const
	{
		return m_sensors.GetAreas();
	}

	bool IsShooting() const
	{
		return m_isShooting;
	}

	bool IsAlive() const
	{
		return m_isAlive;
	}

	bool InForest() const
	{
		return m_inForest;
	}

	void DispDesire() const
	{
		std::cout << "Agent moved to : " << m_desire.GetDestination().ToString() << std::endl;
	}

	/// <summary>
	/// Kill Agent
	/// </summary>
	void Kill();

	void LeaveForest();

	int GetPerformance() const
	{
		return m_performance;
	}

	void SetPerformance(int performance)
	{
		m_performance += performance;
	}

private:
	// AI
	Action m_desire;
	AgentSensors m_sensors;
	AgentEffectors m_effectors;

	// Stats
	Position m_currentAgentPosition;
	bool m_isShooting = false;
	bool m_isAlive = true;
	bool m_inForest = true;
	int m_performance;
};




