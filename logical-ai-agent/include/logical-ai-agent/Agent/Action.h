#pragma once

#include <Position.h>

enum class ActionType { NONE, MOVE, SHOOTANDMOVE };

class Action
{
public:
	Action(ActionType type, Position position) : m_type(type), m_destination(position)
	{
	}
	Action(ActionType type) : Action(type, Position(0,0))
	{
	}
	Action() : Action(ActionType::NONE)
	{
	}

	ActionType GetType() const
	{
		return m_type;
	}

	const Position& GetDestination() const
	{
		return m_destination;
	}

private:
	ActionType m_type;
	Position m_destination;
};