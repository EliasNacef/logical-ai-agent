#pragma once

#include <vector>
#include <Environment/Area.h>

/// <summary>
/// Define an agent environment
/// </summary>
class AgentEnvironment
{
public:

	/// <summary>
	/// Ctor
	/// </summary>
	AgentEnvironment();

	/// <summary>
	/// Add an area to agent environment knwoledge
	/// </summary>
	/// <param name="area"> area to add </param>
	/// <param name="isKnown"> is the agent knows everything about this area ? </param>
	void AddArea(const Area& area, bool isVisited);

private:
	std::vector<std::vector<Area>> m_visitedAreas; // known and visited
	std::vector<std::vector<Area>> m_notVisitedAreas; // known but not visited 
};