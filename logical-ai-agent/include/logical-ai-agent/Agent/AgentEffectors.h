#pragma once
#include <Position.h>
#include <Agent/Action.h>


/// <summary>
/// What could the agent do
/// </summary>
class AgentEffectors
{
public:
	/// <summary>
	/// ctor
	/// </summary>
	AgentEffectors();

	/// <summary>
	/// Use effectors on agent position
	/// </summary>
	/// <param name="desire"> desire </param>
	/// <param name="position"> agent position </param>
	int Use(const Action& desire, Position& agentPosition, bool& isShooting);

	/// <summary>
	/// Update intention and try to do it
	/// </summary>
	/// <param name="desire"> desire </param>
	/// <param name="position"> position </param>
	/// <param name="isShooting"> is agent shooting </param>
	int Do(const Action& desire, Position& position, bool& isShooting);

	/// <summary>
	/// Update intention with desire
	/// </summary>
	void UpdateIntention(const Action& action);

	void SetEnvironmentSize(int size)
	{
		m_envSize = size;
	}

private:
	int m_envSize;
	Action m_intention;

	/// <summary>
	/// Move
	/// </summary>
	void Move(bool& isShooting, Position& position, const Position& destination);

	/// <summary>
	/// Shoot a rock
	/// </summary>
	void ShootAndMove(bool& isShooting, Position& from, const Position& destination);

	/// <summary>
	/// Is position a valid destination ?
	/// </summary>
	/// <param name="position"> position to test </param>
	/// <returns> return validity of position </returns>
	bool IsValid(const Position& position);
};

