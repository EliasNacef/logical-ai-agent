#pragma once
#include <Environment/Area.h>
#include <Agent/Action.h>
#include <InferenceEngine\InferenceEngine.h>
#include <InferenceEngine\AreaFacts.h>

/// <summary>
/// What could the agent observe
/// </summary>
class AgentSensors
{
public:

	/// <summary>
	/// ctor
	/// </summary>
	AgentSensors(std::string rules) : 
		m_envSize(0),
		m_desire(ActionType::NONE),
		m_currentAgentPosition(Position(0, 0)),
		m_engine(InferenceEngine(rules))
	{
	}

	/// <summary>
	/// Return the current agent desire
	/// </summary>
	/// <returns> Agent desire </returns>
	const Action& GetDesire() const
	{
		return m_desire;
	}

	/// <summary>
	/// Use sensors on area to return a desire
	/// </summary>
	/// <param name="area"> area to analyze </param>
	/// <returns> Action desired </returns>
	void Use(const Position& currentPosition, const Area area);

	/// <summary>
	/// Update agent beliefs
	/// </summary>
	/// <param name="area"> Area to observe </param>
	void UpdateBeliefs(const Area area);

	/// <summary>
	/// Update current environment knowledge with area copy : update beliefs
	/// </summary>
	/// <param name="area"> Area to observe </param>
	void ObserveArea(const Area& area);

	
	/// <summary>
	/// Update fringe according to area
	/// </summary>
	/// <param name="area"> center area to update fringe around </param>
	void UpdateFringe(const Area& area);


	/// <summary>
	/// Reset facts with environment knowledge
	/// </summary>
	void ResetAreaFacts();

	/// <summary>
	/// Update facts thanks to inference engine
	/// </summary>
	void InferAreaFacts();

	/// <summary>
	/// Update agent Desire
	/// </summary>
	void UpdateDesire();

	/// <summary>
	/// Return the area at the chosen position
	/// </summary>
	/// <param name="position">the target area's position</param>
	/// <returns>Area at the position</returns>
	const Area& GetArea(const Position& position) const;

	std::vector<Position> GetAround(const Area& area) const;
	std::vector<Position> GetAround(const Position& area) const;

	/// <summary>
	/// Tell if the area is already known by the agent
	/// </summary>
	/// <param name="position">the target area's position</param>
	/// <returns>True if the area is already known, false if not</returns>
	bool IsAlreadyKnown(const Position& position) const;

	/// <summary>
	/// Return all the areas konwn by the agent
	/// </summary>
	/// <returns>A vector with all the known areas</returns>
	std::vector<Area> GetAreas() const
	{
		return m_knownAreas;
	}

	void SetEnvironmentSize(int size)
	{
		m_envSize = size;
	}

	int GetEnvironmentSize() const
	{
		return m_envSize;
	}

private:
	int m_envSize;
	Action m_desire;
	Position m_currentAgentPosition;
	InferenceEngine m_engine;
	std::vector<Area> m_knownAreas;
	std::vector<Position> m_fringePositions;
	std::vector<AreaFacts> m_areaFacts;
	std::vector<AreaFacts> m_inferedFacts;

	float GetFactProbability(const AreaFacts& areaFacts) const;
	const Action& GetFactDesire(const AreaFacts& areaFacts) const;
	const Action& GetRandomAction() const;
};