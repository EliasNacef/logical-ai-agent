#pragma once

#include <vector>
#include <Environment/Area.h>
#include <Agent\Agent.h>
#include <Position.h>

/// <summary>
/// Define environment
/// </summary>
class Environment
{
public:

	/// <summary>
	/// Ctor generating a random environment of size n by n
	/// </summary>
	Environment(int n, Agent* agent);

	// No copy ctor
	Environment(const Environment&) = delete;
	// No move ctor
	Environment(Environment&&) = delete;

	/// <summary>
	/// Add the small of a monster or the wind of a crack in the environment
	/// </summary>
	void AddSmellAndWind();

	/// <summary>
	/// Environment update one time
	/// </summary>
	void Tick();

	/// <summary>
	/// Grow the environment by one
	/// </summary>
	//void Evolution();

	const void SetArea(const Area& area)
	{
		const auto& pos = area.GetPosition();
		Area& currentArea = m_areas[pos.GetX()][pos.GetY()];
		currentArea = area;
	}

	/// <summary>
	/// Get x,y area in environment
	/// </summary>
	/// <param name="x"> x coord </param>
	/// <param name="y"> y coord </param>
	/// <returns> The x,y area in environment </returns>
	const Area& GetArea(int x, int y) const
	{
		if(x < 0 || x >= m_size) throw "x coord incorrect !";
		if(y < 0 || y >= m_size) throw "y coord incorrect !";
		return m_areas[x][y];
	}

	/// <summary>
	/// Get area with position in environment
	/// </summary>
	/// <param name="position"> area coord </param>
	/// <returns> The area at the correct position in environment </returns>
	const Area& GetArea(const Position& position) const
	{
		return GetArea(position.GetX(), position.GetY());
	}


	/// <summary>
	/// Get environment size
	/// </summary>
	/// <returns></returns>
	size_t GetSize() const
	{
		return m_size;
	}

	/// <summary>
	/// Disp the environment in console
	/// </summary>
	void Disp();

private:
	int m_size;
	std::vector<std::vector<Area>> m_areas;

	Agent* m_agent;
	Position m_currentAgentPosition;


	/// <summary>
	/// Update agent position
	/// </summary>
	void UpdateAgentPosition();
	bool IsPositionValid(const Position& pos) const;
	void ArriveOnArea();
	void DispLine() const;
};