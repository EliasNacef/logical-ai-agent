#pragma once

#include <vector>
#include<random>
#include <Position.h>

/// <summary>
/// Define an area in an environment
/// </summary>
class Area
{
public:

	/// <summary>
	/// ctor
	/// </summary>
	/// <param name="position">Area's position</param>
	Area(Position position);

	/// <summary>
	/// ctor
	/// </summary>
	Area() = default;

	/// <summary>
	/// Copy ctor
	/// </summary>
	Area(const Area&) = default;

	/// <summary>
	/// Put monster smell in the area
	/// </summary>
	void ThereIsMonsterSmell()
	{
		m_monsterSmell = true;
	}

	/// <summary>
	/// Put a monster in the area
	/// </summary>
	void ThereIsMonster()
	{
		m_monsterPresence = true;
	}


	/// <summary>
	/// Kill monster in this area
	/// </summary>
	void KillMonster()
	{
		m_monsterPresence = false;
	}

	/// <summary>
	/// kill the monster in the area
	/// </summary>
	void DeleteMonster()
	{
		m_monsterPresence = false;
	}

	/// <summary>
	/// Put crack wind in the area
	/// </summary>
	void ThereIsCrackWind()
	{
		m_crackWind = true;
	}

	/// <summary>
	/// Put a crack in the area
	/// </summary>
	void ThereIsCrack()
	{
		m_crackPresence = true;
	}

	/// <summary>
	/// Delete a crack in the area
	/// </summary>
	void DeleteCrack()
	{
		m_crackPresence = false;
	}

	/// <summary>
	/// Put portal light in the area
	/// </summary>
	void ThereIsPortalLight()
	{
		m_portalLight = true;
	}

	/// <summary>
	/// Put a portal in the area
	/// </summary>
	void ThereIsPortal()
	{
		m_portalPresence = true;
	}

	/// <summary>
	/// Is there a monster smell in this area ?
	/// </summary>
	/// <returns> true if a monster smell is in the area </returns>
	bool IsThereMonsterSmell() const
	{
		return m_monsterSmell;
	}

	/// <summary>
	/// Is there a monster in this area ?
	/// </summary>
	/// <returns> true if a monster is in the area </returns>
	bool IsThereMonster() const
	{
		return m_monsterPresence;
	}

	/// <summary>
	/// Is there some crack wind in this area ?
	/// </summary>
	/// <returns> true if there is some crack wind is in the area </returns>
	bool IsThereCrackWind() const
	{
		return m_crackWind;
	}


	/// <summary>
	/// Is there a crack in this area ?
	/// </summary>
	/// <returns> true if there is a crack in the area </returns>
	bool IsThereCrack() const
	{
		return m_crackPresence;
	}

	/// <summary>
	/// Is there a portal light in this area ?
	/// </summary>
	/// <returns> true if there is a portal light in the area </returns>
	bool IsTherePortalLight() const
	{
		return m_portalLight;
	}

	/// <summary>
	/// Is there a portal in this area ?
	/// </summary>
	/// <returns> true if there is a portal in the area </returns>
	bool IsTherePortal() const
	{
		return m_portalPresence;
	}

	/// <summary>
	/// The agent arrived in the area
	/// </summary>
	void AgentIsHere()
	{
		m_agentInTheArea = true;
	}

	/// <summary>
	/// The agent is not in the area anymore
	/// </summary>
	void AgentIsGone()
	{
		m_agentInTheArea = false;
	}

	/// <summary>
	/// Getter for area position
	/// </summary>
	/// <returns> area position </returns>
	const Position& GetPosition() const
	{
		return m_position;
	}

	/// <summary>
	/// Disp the area in console
	/// </summary>
	void Disp() const;

private:
	// Position
	Position m_position;

	// Monster
	bool m_monsterSmell;
	bool m_monsterPresence;

	// Crack
	bool m_crackWind;
	bool m_crackPresence;

	// Portal
	bool m_portalLight;
	bool m_portalPresence;

	//Agent
	bool m_agentInTheArea;
};